# DWP_linear_opt_math

## LICENSE

[GNU AFFERO GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/agpl-3.0.txt)

## About

### What is this?

This project is where I mess around with some coding related to mathematical (linear) optimization for fun. I also try out various coding practices, designs and patterns as well as language features. All of this project is licensed under the [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/agpl-3.0.txt) license.

### What does it do right now?

It builds threes binaries:
1. ```find_feasible``` that finds a feasible point to an inequality system (basically ```Ax <= b``` where ```x``` is bounded). It starts by reading a linear programming problem from an ```.mps``` file, only caring about ```Ax <= b``` and variables bounds of the problem. The program then asks a [COINOR](http://www.coin-or.org/) library if the point is actually feasible, just as a check.
2. ```benchmark_matrix_vector_multiply``` that is used for benchmarking.
3. ```matrix_math_tests``` which runs unit and property tests. For a first go with the tests, run:
```
matrix_math_tests --gtest_filter="-*slow"
```
to skip slow tests. Some of the property-based tests take a while since they test that property holds for operations on matrices for _many_ sparsity patterns.

### How did this project start?

I spent a lot of time testing Yang and Lin's RSG (Restarted SubGradient) method applied to the Lagrangian dual of general ILPs. For RSG, I needed a lower/upper bound so I coded a proximal gradient algorithm to find a feasible point to the linear relaxation of the ILP. This worked by minimizing the square of the constraint violations. I never got RSG to work well; the constants in the complexity result were a problem in pratice. But I felt that this implementation of the proximal gradient algorithm worked well so I wanted to use it for something.

## CONTACT

Douglas Wayne Potter (douglaspotter [at] gmail (dot) com)

## Useful

### Notes

Out of source build (supported build types: Release, Debug and Sanitize):
```
mkdir build
cd build
cmake  -DCMAKE_BUILD_TYPE="Release" ..
cmake --build .
```

Get newer cmake in order to support C++17 on debian stretch:
1. Add  `deb http://ftp.debian.org/debian stretch-backports main` to `/etc/apt/sources.list` 
2. Run `sudo apt-get -t stretch-backports install cmake`

Install gtest on ubuntu:
```
sudo apt-get install libgtest-dev
cd /usr/src/gtest/
sudo cmake CMakeLists.txt 
sudo make
sudo cp *.a /usr/lib
```

Install Google benchmark:
```
git clone https://github.com/google/benchmark.git
cd benchmark
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
sudo make install
```

Run clang-format on code:
```
find -name "*.hpp" -o -name "*.cpp" | xargs clang-format -i -style=Google
```

Disable cpu scaling:
```
for CPUFREQ in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do [ -f $CPUFREQ ] || continue; echo -n performance > $CPUFREQ; done
grep -E '^model name|^cpu MHz' /proc/cpuinfo
```

Generate data for rtags:
```
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
rc -J
```

Some local links and command examples:

`//usr/share/doc/coinor-libcoinutils-doc/html/index.html`

`//home/douglas/proj/blaze/blaze-docu-3.1/index.html`

`cbc ~/proj/cpp/CoinUtils/coin-CoinUtils/Data/Sample/p0201.mps solve sol.txt`

`clp ~/proj/cpp/CoinUtils/coin-CoinUtils/Data/Sample/p0201.mps`

### Links

[test cases links](http://plato.asu.edu/sub/testcases.html)

[test cases, MIPLIB 3.0 from 1996](http://miplib.zib.de/miplib3/miplib.html)

[test cases, MIPLIP 2003](http://miplib.zib.de/miplib2003/index.php)

### TODO
stop using #pragma and instead use regular head guards

flush denormals?

check that move constructors are used

-fno-omit-frame-pointer

maybe use free functions of begin, end, cbegin, cend and so on.

do some computations "via logaritms" to avoid and detect overflow problems

try some proximal algorithms
ex. using back tracking in "Gradient-based algorithms with applications to signal recovery problems"

leave some asserts active in release build like those checking dims of matrices and vectors

for any executables that are neither for testing nor benchmarking, add try/catch in main

consider having one matrix where we sort the rows into equality constraints and inequality constraints instead of two (one for equality constraints and one for inequality constraints). This would probably make the code less clean but could improve performance. profile.

use noexcept and inline more and more consistently

document quirk: that A and C must have same minor size, even if one is empty.

do something about that it is too easy to forget to finish a major when working MajorMinorMatrix

thinking about normalizing constraint matrices even more, so the Lipschitz constant is one for the feasibility problem

make up my mind about which parts should be regular static libraries and which parts should be "header only".

---

Copyright (C) 2018 Douglas W. Potter
