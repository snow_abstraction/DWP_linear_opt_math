cmake_minimum_required(VERSION 3.5)

add_library(problem_definitions INTERFACE)

target_include_directories(problem_definitions  INTERFACE .)
