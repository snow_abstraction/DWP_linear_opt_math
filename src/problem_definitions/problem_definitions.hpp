// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "MajorMinorMatrix.hpp"

#include <assert.h>

namespace ppilps {
namespace optimization {

// TwoSidedVariableBounds represents the bounds on a variable
// that is bound from below by the lb (lower bound) and the above
// by the ub (upper bound).
struct TwoSidedVariableBounds {
  TwoSidedVariableBounds(double const lb_in, double const ub_in)
      : lb(lb_in), ub(ub_in) {
    assert(lb <= ub);
  }

  double const lb = 0;
  double const ub = 0;
};

// LPInstance represents an instance of the following minimization problem
// min cx
// subject to
// Ax =  b
// Cx <= d
// where
// each x_i is bounded.
struct LPInstance {
  // TODO add constructor that moves in data members

  LPInstance(std::vector<double> const &c_in,
             sparse_math::MajorMinorMatrix const &A_in,
             std::vector<double> const &b_in,
             sparse_math::MajorMinorMatrix const &C_in,
             std::vector<double> const &d_in,
             std::vector<TwoSidedVariableBounds> const &variable_bounds_in)
      : c(c_in),
        A(A_in),
        b(b_in),
        C(C_in),
        d(d_in),
        variable_bounds(variable_bounds_in) {
    assert(A.get_minor_size() == c.size());
    assert(A.get_major_size() == b.size());
    assert(C.get_major_size() == d.size());
    assert(A.get_minor_size() == C.get_minor_size());
    assert(A.get_minor_size() == variable_bounds.size());
  }

  std::vector<double> c;
  sparse_math::MajorMinorMatrix const A;
  std::vector<double> const b;
  sparse_math::MajorMinorMatrix const C;
  std::vector<double> const d;
  std::vector<TwoSidedVariableBounds> const variable_bounds;
};

// LinearFeasiabilityProblemInstance does not own its own data.
// It is an instance of the following
// systems of linear constraints:
// Ax =  b
// Cx <= d
// where
// each x_i is bounded.
struct LinearFeasibilityProblemInstance {
  LinearFeasibilityProblemInstance(
      sparse_math::MajorMinorMatrix const &A_in,
      std::vector<double> const &b_in,
      sparse_math::MajorMinorMatrix const &C_in,
      std::vector<double> const &d_in,
      std::vector<TwoSidedVariableBounds> const &variable_bounds_in)
      : A(A_in),
        b(b_in),
        C(C_in),
        d(d_in),
        variable_bounds(variable_bounds_in) {
    assert(A.get_major_size() == b.size());
    assert(C.get_major_size() == d.size());
    assert(A.get_minor_size() == C.get_minor_size());
    assert(A.get_minor_size() == variable_bounds.size());
  }
  sparse_math::MajorMinorMatrix const &A;
  std::vector<double> const &b;
  sparse_math::MajorMinorMatrix const &C;
  std::vector<double> const &d;
  std::vector<TwoSidedVariableBounds> const &variable_bounds;
};
}
}
