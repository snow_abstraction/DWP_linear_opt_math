// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include <iostream>
#include <ostream>
#include <utility>

namespace ppilps {
namespace logging {

namespace {
template <class T>
struct Expr {
  T const &value;
  char const *const name;
};

template <class T>
inline std::ostream &operator<<(std::ostream &os, Expr<T> x) {
  os << x.name << '[' << x.value << ']';
  return os;
}
}

#define LOG_EXPR(x) \
  Expr<decltype(x)> { x, #x }

// The class Log is a base class for simple loggers. It supports two ways of
// logging:
// 1. using the output operator << like std::cout, and
// 2. calling log_comma_separated to log any number of arguments, output
// comman-separated.
// In both cases, << needs to found for the type so if call use to std::cout for
// the type
// the logger supports logging it.
//
// The intention is that it should be easy to:
// 1. switch between using a LogBase logger and std::cout if all logging
//    is done via << (should only require a litte sed), and
// 2. easy to disable logging by using a DevNullLog instance
//
// One convenience is by using the evil macro LOG_EXPR above, you can write:
//
// int a = 10;
// std::string c("asdf");
// log << "some values: ";
// log.log_comma_separated(LOG_EXPR(a), LOG_EXPR(c));
//
// to get "some values: a[10], c[asdf]" logged.

class LogBase {
  // used to keep track if we should call output_prefix or not
  bool use_prefix = true;

 protected:
  virtual std::ostream &get_os() = 0;
  virtual void endl_impl() = 0;
  virtual void flush_impl(){};
  // Called before first output and before first output on a new line.
  virtual void output_prefix() {}
  virtual void end_entry_impl() { get_os() << '\n'; }

 public:
  using LogBase_to_LogBase_func = LogBase &(LogBase &);
  virtual ~LogBase() {}

  template <class T>
  void log_comma_separated(T &&v) {
    get_os() << std::forward<T>(v);
  }

  // FUTURE: C++17 replace with fold expression
  template <class T, class... Args>
  void log_comma_separated(T &&first, Args... args) {
    log_comma_separated(std::forward<T>(first));
    get_os() << ", ";
    log_comma_separated(args...);
  }

  // For simplicity and consistency, we do not delagate the actually logging
  // to a concrete instance. Instead we rely on ostream's << operator.
  // Also it would be tricky to do since C++
  // does not support virtual template member functions.
  template <class T>
  void log(T &&v) {
    if (use_prefix) {
      output_prefix();
      use_prefix = false;
    }
    get_os() << std::forward<T>(v);
  }

  void endl() {
    endl_impl();
    use_prefix = true;
  }

  void flush() { flush_impl(); }

  void end_entry() {
    end_entry_impl();
    use_prefix = true;
  }
};

template <class T>
inline LogBase &operator<<(LogBase &l, T &&v) {
  l.log(v);
  return l;
}

inline LogBase &operator<<(LogBase &l, LogBase::LogBase_to_LogBase_func f) {
  return f(l);
}

inline LogBase &endl(LogBase &l) {
  l.endl();
  return l;
}

inline LogBase &flush(LogBase &l) {
  l.flush();
  return l;
}

inline LogBase &end_entry(LogBase &l) {
  l.end_entry();
  return l;
}

// A logger for logging to standard out.
class CoutLog : public LogBase {
  std::ostream &get_os() override { return std::cout; }
  void endl_impl() override { get_os() << std::endl; }
  void flush_impl() override { get_os() << std::flush; }
  void output_prefix() override { get_os() << prefix; }
  std::string prefix;

 public:
  CoutLog(std::string prefix_in = std::string()) : prefix(prefix_in) {}
};

// A logger for not outputing log messages, that is disabling logging.
class DevNullLog : public LogBase {
  std::ostream &get_os() override { return os; }
  void endl_impl() override {}
  std::ostream os;

 public:
  // C++11 standard 27.7.3.2 [ostream.cons] says if a std::ostream is
  // constructed like this then it will silently ignore all writes.
  DevNullLog(std::string str = {}) : os(nullptr) {}
};
}
}
