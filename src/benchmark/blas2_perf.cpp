// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "MajorMinorMatrix.hpp"
#include "operations.hpp"

#include "benchmark/benchmark.h"

#include <cassert>
#include <limits>
#include <map>
#include <random>
#include <tuple>
#include <vector>

namespace ppilps {
namespace Benchmark {
using namespace sparse_math;

using MatrixArgs = std::tuple<int, int, int>;

class Blas2Perf : public benchmark::Fixture {
 private:
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution_01;
  std::uniform_real_distribution<double> distribution_real;

 public:
  using InputDatum =
      std::tuple<MajorMinorMatrix, std::vector<double>, std::vector<double>>;

  using InputData = std::map<Index, std::map<Index, std::map<int, InputDatum>>>;

  const InputData data;

  Blas2Perf()
      : generator(1),
        distribution_01(0.0, 1.0),
        distribution_real(std::numeric_limits<double>::lowest() / 2 + 1,
                          std::numeric_limits<double>::max() / 2 - 1),
        data(generate_matrices()) {}

  static std::vector<std::tuple<int, int, int>> generate_arguments() {
    std::vector<std::tuple<int, int, int>> rv;

    for (long unsigned int i = 128; i <= 1 << 20;) {
      for (long unsigned int j = 128; j <= 1 << 20;) {
        if (i * j < 1 << 28) {
          int const major_size = i;
          int const minor_size = j;
          assert(static_cast<long unsigned int>(major_size) == i);
          assert(static_cast<long unsigned int>(minor_size) == j);
          rv.emplace_back(major_size, minor_size, 10);
          rv.emplace_back(major_size, minor_size, 100);
          rv.emplace_back(major_size, minor_size, 10000);
        }

        j = increase(j);
      }

      i = increase(i);
    }

    return rv;
  }

  MajorMinorMatrix const &get_length_based_test_matrix(
      int const major_size, int const minor_size,
      int const sparseness_probability_divisor) {
    return std::get<0>(
        get_test_datum(major_size, minor_size, sparseness_probability_divisor));
  }

  std::vector<double> const &get_x_vector(
      int const major_size, int const minor_size,
      int const sparseness_probability_divisor) {
    return std::get<1>(
        get_test_datum(major_size, minor_size, sparseness_probability_divisor));
  }

  std::vector<double> const &get_b_vector(
      int const major_size, int const minor_size,
      int const sparseness_probability_divisor) {
    return std::get<2>(
        get_test_datum(major_size, minor_size, sparseness_probability_divisor));
  }

 private:
  template <class T>
  static long int increase(T const x) {
    T rv = x;
    if (rv < 1024) {
      rv *= 4;
    } else {
      rv *= rv;
    }

    assert(x < rv);

    return rv;
  }

  InputData generate_matrices() {
    using std::get;
    InputData rv;

    for (const auto &args : Blas2Perf::generate_arguments()) {
      rv[get<0>(args)][get<1>(args)][get<2>(args)] =
          generate_BenchmarkInputData(args);
    }

    return rv;
  }

  InputDatum generate_BenchmarkInputData(MatrixArgs const args) {
    using std::get;

    Index const major_size = get<0>(args);
    Index const minor_size = get<1>(args);
    int const sparseness_probability_divisor = get<2>(args);

    const double expected_spareness =
        1.0 / static_cast<double>(sparseness_probability_divisor);

    MajorMinorMatrix m;

    // make sure that for the same dimensions
    // that sparser matrix has fewer or equal nnz
    generator.seed(major_size - minor_size);

    for (Index i = 0; i < major_size; ++i) {
      for (Index j = 0; j < minor_size; ++j) {
        auto sparseness_outcome = distribution_01(generator);
        if (sparseness_outcome < expected_spareness) {
          auto element_value = distribution_real(generator);
          m.add_element_to_current_major(j, element_value);
        }
      }
      m.finish_major();
    }

    m.increase_minor_size_to(minor_size);

    std::vector<double> v(minor_size);
    for (auto &el : v) {
      el = distribution_real(generator);
    }

    std::vector<double> b(minor_size);
    for (auto &el : b) {
      el = distribution_real(generator);
    }

    return InputDatum(m, v, b);
  }

  InputDatum const &get_test_datum(int const major_size, int const minor_size,
                                   int const sparseness_probability_divisor) {
    auto major_it = data.find(major_size);
    assert(major_it != data.cend());
    auto major_minor_it = major_it->second.find(minor_size);
    assert(major_minor_it != major_it->second.cend());
    auto major_minor_divisor_it =
        major_minor_it->second.find(sparseness_probability_divisor);
    assert(major_minor_divisor_it != major_minor_it->second.cend());

    return major_minor_divisor_it->second;
  }
};

BENCHMARK_DEFINE_F(Blas2Perf, length_matrix_vector_multiply)
(benchmark::State &state) {
  auto &m = get_length_based_test_matrix(state.range(0), state.range(1),
                                         state.range(2));
  const auto &x = get_x_vector(state.range(0), state.range(1), state.range(2));

  std::vector<double> output(m.get_major_size());

  while (state.KeepRunning()) {
    multiply(m, x, output);
  }
}

static void CustomArguments(benchmark::internal::Benchmark *b) {
  for (const auto &args : Blas2Perf::generate_arguments()) {
    b->Args({std::get<0>(args), std::get<1>(args), std::get<2>(args)});
  }
}

// If linker problems, maybe move outside of namespaces and
// specify namespaces for args explicitly
BENCHMARK_REGISTER_F(Blas2Perf, length_matrix_vector_multiply)
    ->Apply(CustomArguments);
}
}

BENCHMARK_MAIN();
