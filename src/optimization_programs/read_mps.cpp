// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical
// optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "MajorMinorMatrix.hpp"
#include "problem_definitions.hpp"

#include "coin/CoinMpsIO.hpp"

#include <cassert>
#include <stdexcept>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

namespace ppilps {
namespace Util {

using namespace sparse_math;
using namespace ppilps::optimization;

struct ConstraintsData {
  MajorMinorMatrix A;
  std::vector<double> b;
  MajorMinorMatrix C;
  std::vector<double> d;
};

struct RowInstructions {
  double multiplier = 0;
  bool is_equality = true;
};

void throw_if_unrepresentable_as_int(double x) {
  if (static_cast<double>(static_cast<int>(x)) != x) {
    std::stringstream ss;
    ss << "Value " << x << " encounted but only integer values are supported.";
    throw std::domain_error(ss.str());
  }
}

RowInstructions compute_row_instructions(double const abs_multiplier,
                                         char const sense) {
  RowInstructions ins;
  switch (sense) {
    case 'E':
      ins.multiplier = abs_multiplier;
      ins.is_equality = true;
      break;
    case 'L':
      ins.multiplier = abs_multiplier;
      ins.is_equality = false;
      break;
    case 'G':
      ins.multiplier = -abs_multiplier;
      ins.is_equality = false;
      break;
    default:
      assert(0 && "unsupported sense");
  }

  return ins;
}

struct NumberOfEachRowType {};

void reserve_rhs(CoinMpsIO const &mps_io, std::vector<double> &equality_rhs,
                 std::vector<double> &inequality_rhs) {
  int num_equality_rows = 0;
  int num_inequality_rows = 0;

  // count E(quality) rows and L(less than or equal) and G(greater than
  // or equal) rows (G rows will be transformed into L rows)
  for (int j = 0; j < mps_io.getNumRows(); ++j) {
    char const &sense = *(mps_io.getRowSense() + j);
    if (sense == 'E') {
      ++num_equality_rows;
    } else if (sense == 'L' || sense == 'G') {
      ++num_inequality_rows;
    } else {
      assert(0 && "Unsupport constraint type");
    }
  }

  equality_rhs.reserve(num_equality_rows);
  inequality_rhs.reserve(num_inequality_rows);
}

// The normalization changes the basis of the constraint matrices by scaling
// rows. The scaling
// is for algorithms but it done while reading from the mps to avoid copies. The
// scaling
// should reduce the lipschitz constant (increasing the step size) in the
// algorithms and make
// level sets more round and less like skewed ellipsoids.
ConstraintsData create_constraints_from_mps_with_normalized_rows(
    CoinMpsIO const &mps_io,
    std::function<double(double const *row_begin, double const *row_end)> const
        &compute_row_normalization_factor) {
  MajorMinorMatrix A;
  std::vector<double> b;
  MajorMinorMatrix C;
  std::vector<double> d;
  reserve_rhs(mps_io, b, d);

  auto const &matrix = mps_io.getMatrixByRow();
#ifndef NDEBUG
  auto const nnz = matrix->getNumElements();
#endif
  // The source code for getMajorindices and Clang's and gcc's address
  // sanitizers
  // say that we need to take care of this memory's deallocation.
  std::unique_ptr<const int[]> const row_indices(matrix->getMajorIndices());
  int const *const col_indices = matrix->getIndices();
  double const *const elements = matrix->getElements();

  assert(matrix->getNumRows() <= matrix->getSizeVectorStarts());
  assert(matrix->getNumRows() <= matrix->getSizeVectorLengths());
  for (int row_idx = 0; row_idx < matrix->getNumRows(); ++row_idx) {
    auto const row_start_idx = matrix->getVectorStarts()[row_idx];
    auto const row_len = matrix->getVectorLengths()[row_idx];
    auto const row_begin = elements + row_start_idx;
    auto const row_end = elements + row_start_idx + row_len;
    assert(row_len == 0 || row_indices[row_start_idx] == row_idx);

    auto const abs_multiplier =
        compute_row_normalization_factor(row_begin, row_end);
    auto const instructions =
        compute_row_instructions(abs_multiplier, mps_io.getRowSense()[row_idx]);

    double const &rhs_element = mps_io.getRightHandSide()[row_idx];
    throw_if_unrepresentable_as_int(rhs_element);
    auto &fill_rhs = instructions.is_equality ? b : d;
    fill_rhs.push_back(instructions.multiplier * rhs_element);

    MajorMinorMatrix &fill_matrix = instructions.is_equality ? A : C;

    for (int idx = row_start_idx; idx < row_start_idx + row_len; ++idx) {
      auto const &value = elements[idx];
      throw_if_unrepresentable_as_int(value);
      fill_matrix.add_element_to_current_major(col_indices[idx],
                                               instructions.multiplier * value);
      assert(idx < nnz);
      assert(row_indices[idx] == row_idx);
    }

    fill_matrix.finish_major();
  }

  assert((A.get_major_size() != 0 || C.get_major_size() != 0) &&
         "Both the matrics for equality and inequality constraints are empty, "
         "this is an error");

  A.increase_minor_size_to(mps_io.getNumCols());
  C.increase_minor_size_to(mps_io.getNumCols());
  assert(A.get_major_size() == b.size());
  assert(C.get_major_size() == d.size());

  return ConstraintsData{A, b, C, d};
}

LPInstance read_mps(
    std::string const &mps_path,
    std::function<double(double const *row_begin, double const *row_end)> const
        &compute_row_normalization_factor) {
  // ClpSimplex interface was not easy to query so
  // use CoinMpsIO to get the data for building matrices
  CoinMpsIO mps_io;
  {
    auto rc = mps_io.readMps(mps_path.c_str(), "");
    if (rc == -1) {
      throw std::runtime_error("Error reading mps file for CoinMpsIO.");
    }
  }

  auto constraints_data = create_constraints_from_mps_with_normalized_rows(
      mps_io, compute_row_normalization_factor);

  std::vector<double> objective_function(
      mps_io.getObjCoefficients(),
      mps_io.getObjCoefficients() + mps_io.getNumCols());
  assert(constraints_data.A.get_minor_size() == objective_function.size());
  for (auto const &obj_coeff : objective_function) {
    throw_if_unrepresentable_as_int(obj_coeff);
  }

  std::vector<TwoSidedVariableBounds> variable_bounds;
  variable_bounds.reserve(mps_io.getNumCols());
  for (int col_idx = 0; col_idx < mps_io.getNumCols(); ++col_idx) {
    auto const lb = mps_io.getColLower()[col_idx];
    auto const ub = mps_io.getColUpper()[col_idx];
    throw_if_unrepresentable_as_int(lb);
    throw_if_unrepresentable_as_int(ub);
    variable_bounds.emplace_back(lb, ub);
  }

  assert(constraints_data.A.get_minor_size() == variable_bounds.size());

  LPInstance instance = {objective_function, constraints_data.A,
                         constraints_data.b, constraints_data.C,
                         constraints_data.d, variable_bounds};

  return instance;
}
}
}
