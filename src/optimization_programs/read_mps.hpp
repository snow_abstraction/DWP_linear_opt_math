// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical
// optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "problem_definitions.hpp"

#include <functional>
#include <string>

namespace ppilps {
namespace Util {

ppilps::optimization::LPInstance read_mps(
    std::string const &mps_path,
    std::function<double(double const *row_begin, double const *row_end)> const
        &compute_row_normalization_factor = [](auto, auto) { return 1.0; });
}
}
