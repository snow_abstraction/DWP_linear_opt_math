// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "minimization_algorithms.hpp"
#include "problem_definitions.hpp"
#include "read_mps.hpp"

#include "logging.hpp"

#include "coin/ClpSimplex.hpp"

#include <iostream>
#include <stdexcept>

int main(int argc, char **argv) {
  using namespace ppilps::Util;
  using namespace ppilps::optimization;
  using namespace ppilps::logging;

  CoutLog log("find_feasible_log: ");

  if (argc != 2) {
    std::cerr << "Please supply on the mps file name\n";
    return -1;
  }

  try {
    ClpSimplex clp;
    {
      auto rc = clp.readMps(argv[1], "");
      if (rc == -1) {
        throw std::runtime_error("Error reading mps file for ClpSimplex.");
      }
    }

    assert(0 < clp.getNumRows());
    auto const num_rows_factor =
        1.0 / std::sqrt(static_cast<double>(clp.getNumRows()));
    auto row_normalization_func = [num_rows_factor](double const *begin,
                                                    double const *end) {
      double const row_inner_product =
          std::inner_product(begin, end, begin, 0.0);
      return 0 < row_inner_product
                 ? num_rows_factor / std::sqrt(row_inner_product)
                 : 1.0;
    };

    LPInstance instance = read_mps(argv[1], row_normalization_func);

    double const num_variables_inverse =
        1.0 / static_cast<double>(instance.variable_bounds.size());
    double const mean_variable_width = std::accumulate(
        instance.variable_bounds.cbegin(), instance.variable_bounds.cend(), 0.0,
        [&](double const &acc, TwoSidedVariableBounds const &bound) {
          return acc + num_variables_inverse * (bound.ub - bound.lb);
        });

    log.log_comma_separated(LOG_EXPR(mean_variable_width));
    log << end_entry;

    LinearFeasibilityProblemInstance feasiblility_instance(
        instance.A, instance.b, instance.C, instance.d,
        instance.variable_bounds);

    log << "A.get_nnz()[" << instance.A.get_nnz() << "]" << end_entry;
    log << "C.get_nnz()[" << instance.C.get_nnz() << "]" << end_entry;

    auto rv = find_feasible_point(feasiblility_instance, 1e-13, 1e-8, log, 1e7);
    log << (rv.feasible_point_found.empty() ? "no feasible point found"
                                            : "feasible point found")
        << end_entry;
    if (!rv.feasible_point_found.empty()) {
      clp.setColSolution(rv.feasible_point_found.data());
    }
    clp.checkSolution();

    log << "objective value[" << clp.objectiveValue() << "]" << end_entry;
    log << "sum primal infeasiblity[" << clp.sumPrimalInfeasibilities() << "]"
        << end_entry;
    log << "number of unsatsified constraints["
        << clp.numberPrimalInfeasibilities() << "]" << end_entry;

  } catch (std::exception const &e) {
    std::cerr << "The program has terminated since an error was encountered: "
              << e.what() << std::endl;
    return -1;
  }

  return 0;
}
