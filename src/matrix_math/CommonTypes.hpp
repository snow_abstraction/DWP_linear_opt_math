// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include <cstdint>
#include <limits>
#include <vector>

// TODO: rename this file or maybe move types since they are not shared between
// / used in two matrices classes anymore

namespace ppilps {
namespace sparse_math {

// either an index (usually a minor row/column index) or nnz (number of nonzero)
// elements in major row/column
using Index = uint32_t;
using Value = double;

const Index limit_index = std::numeric_limits<Index>::max();

struct IndexValue {
  IndexValue() = default;
  IndexValue(Index index, Value value) : i(index), v(value) {}

  Index i = 0;
  Value v = 0.0;
};

inline bool operator==(const IndexValue& lhs, const IndexValue& rhs) {
  return lhs.i == rhs.i && lhs.v == rhs.v;
}

inline bool operator!=(const IndexValue& lhs, const IndexValue& rhs) {
  return !(lhs == rhs);
}
}
}
