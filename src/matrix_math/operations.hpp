// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "CommonTypes.hpp"
#include "MajorMinorMatrix.hpp"

#include <cassert>
#include <numeric>

namespace ppilps {
namespace sparse_math {

// Peforms f( (Ax)_j, b_j, output_j)
// consider the dimensions to be:
// A = (m, n),
// b = (n ,1)
// x = (n, 1) and output = (m, 1)
// with A as row matrix
// partially unrolled for simd instructions.
// Note: m = 0 is supported and then output should not be changed.
template <class F>
inline void Ax_plus_b_base(MajorMinorMatrix const &A,
                           std::vector<double> const &x,
                           std::vector<double> const &b, F f,
                           std::vector<double> &output) noexcept {
  assert(A.get_minor_size() == x.size());
  assert(A.get_major_size() == output.size());
  assert(b.size() == output.size());

  auto const el_data_end_it = A.get_data().cend() - 1;
  assert(el_data_end_it->i == 0);

  const Index num_majors = A.get_major_size();
  auto data_it = A.get_data().cbegin();

  Index major_index = 0;
  for (; data_it != el_data_end_it; ++major_index) {
    Value output_value = 0.0;
    Index nnz = data_it->i;
    ++data_it;
    if (nnz != 0) {
      auto const minor_end_it = data_it + nnz;

      for (; minor_end_it - data_it > 3; data_it += 4) {
        assert(data_it + 3 < A.get_data().size() + A.get_data().cbegin());
        auto matrix_value0 = (data_it + 0)->v;
        auto vector_value0 = x[(data_it + 0)->i];
        auto matrix_value1 = (data_it + 1)->v;
        auto vector_value1 = x[(data_it + 1)->i];
        auto matrix_value2 = (data_it + 2)->v;
        auto vector_value2 = x[(data_it + 2)->i];
        auto matrix_value3 = (data_it + 3)->v;
        auto vector_value3 = x[(data_it + 3)->i];

        output_value +=
            matrix_value0 * vector_value0 + matrix_value1 * vector_value1 +
            matrix_value2 * vector_value2 + matrix_value3 * vector_value3;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
      for (; data_it != minor_end_it;) {
        output_value += data_it->v * x[data_it->i];
        ++data_it;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
    }
    f(output_value, b[major_index], output[major_index]);
  }

  for (; major_index < num_majors; ++major_index) {
    f(0, b[major_index], output[major_index]);
  }
}

inline void Ax_minus_b(MajorMinorMatrix const &A, std::vector<double> const &x,
                       std::vector<double> const &b,
                       std::vector<double> &output) {
  Ax_plus_b_base(A, x, b, [](double const &ax_j, double const &b_j,
                             double &output_j) { output_j = ax_j - b_j; },
                 output);
}

inline void Ax_plus_b(MajorMinorMatrix const &A, std::vector<double> const &x,
                      std::vector<double> const &b,
                      std::vector<double> &output) {
  Ax_plus_b_base(A, x, b, [](double const &ax_j, double const &b_j,
                             double &output_j) { output_j = ax_j + b_j; },
                 output);
}

inline void max_0_and_Ax_minus_b(MajorMinorMatrix const &A,
                                 std::vector<double> const &x,
                                 std::vector<double> const &b,
                                 std::vector<double> &output) {
  Ax_plus_b_base(A, x, b,
                 [](double const &ax_j, double const &b_j, double &output_j) {
                   output_j = std::max(0.0, ax_j - b_j);
                 },
                 output);
}

inline void output_plus_Ax_plus_b(MajorMinorMatrix const &A,
                                  std::vector<double> const &x,
                                  std::vector<double> const &b,
                                  std::vector<double> &output) {
  Ax_plus_b_base(A, x, b, [](double const &ax_j, double const &b_j,
                             double &output_j) { output_j += ax_j + b_j; },
                 output);
}

// Peforms f( (Ax)_j, output_j)
// consider the dimensions to be:
// A = (m, n),
// x = (n, 1) and output = (m, 1)
// with A as a row matrix.
// partially unrolled for simd instructions
// Note: m = 0 is supported and then output should not be changed.
template <class F>
inline void Ax_base(MajorMinorMatrix const &A, std::vector<double> const &x,
                    F f, std::vector<double> &output) noexcept {
  assert(A.get_minor_size() == x.size());
  assert(A.get_major_size() == output.size());

  auto const el_data_end_it = A.get_data().cend() - 1;
  assert(el_data_end_it->i == 0);

  const Index num_majors = A.get_major_size();
  auto data_it = A.get_data().cbegin();

  Index major_index = 0;
  for (; data_it != el_data_end_it; ++major_index) {
    Value output_value = 0.0;
    Index nnz = data_it->i;
    ++data_it;
    if (nnz != 0) {
      auto const minor_end_it = data_it + nnz;

      for (; minor_end_it - data_it > 3; data_it += 4) {
        assert(data_it + 3 < A.get_data().size() + A.get_data().cbegin());
        auto matrix_value0 = (data_it + 0)->v;
        auto vector_value0 = x[(data_it + 0)->i];
        auto matrix_value1 = (data_it + 1)->v;
        auto vector_value1 = x[(data_it + 1)->i];
        auto matrix_value2 = (data_it + 2)->v;
        auto vector_value2 = x[(data_it + 2)->i];
        auto matrix_value3 = (data_it + 3)->v;
        auto vector_value3 = x[(data_it + 3)->i];

        output_value +=
            matrix_value0 * vector_value0 + matrix_value1 * vector_value1 +
            matrix_value2 * vector_value2 + matrix_value3 * vector_value3;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
      for (; data_it != minor_end_it;) {
        output_value += data_it->v * x[data_it->i];
        ++data_it;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
    }
    f(output_value, output[major_index]);
  }

  for (; major_index < num_majors; ++major_index) {
    f(0, output[major_index]);
  }
}

// TODO: test
inline void output_plus_Ax(MajorMinorMatrix const &A,
                           std::vector<double> const &x,
                           std::vector<double> &output) {
  Ax_base(A, x, [](double const &ax_j, double &output_j) { output_j += ax_j; },
          output);
}

// Assign: Ax -> output
// consider the dimensions to be A = (m, n), x = (n, 1) and output = (m, 1)
// with A as row matrix
// partially unrolled for simd instructions
inline void multiply(MajorMinorMatrix const &A, std::vector<double> const &x,
                     std::vector<double> &output) noexcept {
  assert(A.get_minor_size() == x.size());
  assert(A.get_major_size() == output.size());
  auto const el_data_end_it = A.get_data().cend() - 1;
  assert(el_data_end_it->i == 0);

  const Index num_majors = A.get_major_size();
  auto data_it = A.get_data().cbegin();

  Index major_index = 0;
  for (; data_it != el_data_end_it; ++major_index) {
    Value output_value = 0.0;
    Index nnz = data_it->i;
    ++data_it;
    if (nnz != 0) {
      auto const minor_end_it = data_it + nnz;

      for (; minor_end_it - data_it > 3; data_it += 4) {
        assert(data_it + 3 < A.get_data().size() + A.get_data().cbegin());
        auto matrix_value0 = (data_it + 0)->v;
        auto vector_value0 = x[(data_it + 0)->i];
        auto matrix_value1 = (data_it + 1)->v;
        auto vector_value1 = x[(data_it + 1)->i];
        auto matrix_value2 = (data_it + 2)->v;
        auto vector_value2 = x[(data_it + 2)->i];
        auto matrix_value3 = (data_it + 3)->v;
        auto vector_value3 = x[(data_it + 3)->i];

        output_value +=
            matrix_value0 * vector_value0 + matrix_value1 * vector_value1 +
            matrix_value2 * vector_value2 + matrix_value3 * vector_value3;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
      for (; data_it != minor_end_it;) {
        output_value += data_it->v * x[data_it->i];
        ++data_it;
        assert(data_it < A.get_data().size() + A.get_data().cbegin());
      }
    }
    output[major_index] = output_value;
  }

  for (; major_index < num_majors; ++major_index) {
    output[major_index] = 0;
  }
}

// Compute square of the frobenius matrix norm of A
// note: This is not used for performance sensitive regions
// so no loop unrolling for SIMD.
//
// TODO: detect overflow by computing it by
// M^2 * nnz^2 * ( sum (|a_ij|/(M * nnz))^2 ) where
// M = max |a_ij|
inline double compute_square_of_forbenius_norm(
    MajorMinorMatrix const &A) noexcept {
  auto const el_data_end_it = A.get_data().cend() - 1;
  assert(el_data_end_it->i == 0);

  Value rv = 0.0;

  for (auto data_it = A.get_data().cbegin(); data_it != el_data_end_it;
       ++data_it) {
    for (Index nnz = data_it->i; nnz > 0; --nnz) {
      ++data_it;
      rv += data_it->v * data_it->v;
    }
  }

  return rv;
}
}
}
