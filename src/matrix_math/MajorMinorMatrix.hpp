// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "CommonTypes.hpp"

#include <algorithm>
#include <cassert>
#include <numeric>
#include <stdexcept>

namespace ppilps {
namespace sparse_math {

// TODO: move free functions and function defs to another file, maybe keep time
// critical stuff in header files

class MajorMinorMatrix {
 public:
  using Storage = std::vector<IndexValue>;

 private:
  Index major_size = 0;
  Index minor_size = 0;
  uint64_t nnz = 0;
  Storage::size_type offset_to_length_of_current_major = 0;

  // for error checking: For a row based matrix, false would be
  // adding elements to a row but never saying where the row ends.
  bool no_major_with_elements_unfinished = true;

  Storage data;

  MajorMinorMatrix(Index major_size_in, Index minor_size_in, uint64_t nnz_in,
                   Storage::size_type offset_to_length_of_current_major_in,
                   bool no_major_with_elements_unfinished_in, Storage &&data_in)
      : major_size(major_size_in),
        minor_size(minor_size_in),
        nnz(nnz_in),
        offset_to_length_of_current_major(offset_to_length_of_current_major_in),
        no_major_with_elements_unfinished(no_major_with_elements_unfinished_in),
        data(data_in) {}

 public:
  MajorMinorMatrix() : data(1) {}

  MajorMinorMatrix(size_t expected_nnz, size_t expected_major_size) {
    size_t num_reserve = expected_nnz + expected_major_size + 1;

    if (num_reserve < expected_nnz) {
      throw std::overflow_error(
          "Could not reserve (expected_nnz + expected_major_size + 1) since "
          "this sum overflowed.");
    }

    data.reserve(num_reserve);
    data.emplace_back();
  }

  void clear() {
    major_size = 0;
    minor_size = 0;
    nnz = 0;
    no_major_with_elements_unfinished = true;
    offset_to_length_of_current_major = 0;
    data.clear();

    data.emplace_back();
  }

  Index get_major_size() const { return major_size; }

  Index get_minor_size() const { return minor_size; }

  uint64_t get_nnz() const { return nnz; }

  void increase_major_size_to(Index new_size) {
    if (new_size < major_size) {
      throw std::invalid_argument(
          "The new size must be greater or equal to the current major size.");
    }

    while (major_size < new_size) {
      finish_major();
    }
  }

  void increase_minor_size_to(Index new_size) {
    if (new_size < minor_size) {
      throw std::invalid_argument(
          "The new size must be greater or equal to the current minor size.");
    }

    minor_size = new_size;
  }

  Storage const &get_data() const {
    assert(no_major_with_elements_unfinished && "Data is in a bad state.");
    return data;
  }

  // For a row/column matrix, this would add an element to
  // the current row/column. The index i must be greater than
  // the last element added if any in the current row/column.
  // The first current row/column is the 0th.
  // Adding the default value (0.0) is a way
  // to increase the size of the minor dimension.
  // Returns true if no error
  void add_element_to_current_major(Index const minor_index, Value v) {
    if (data[offset_to_length_of_current_major].i != 0 &&
        data.back().i >= minor_index) {
      throw std::invalid_argument(
          "Minor elements must be added in order so there indices increase.");
    }

    no_major_with_elements_unfinished = false;

    constexpr Value zero{};
    if (v != zero) {
      ++nnz;
      data.emplace_back(minor_index, v);
      ++data[offset_to_length_of_current_major].i;
    }

    minor_size = std::max(minor_size, minor_index + 1);
  }

  // For a row/column major matrix, this finishes a row/column.
  void finish_major() {
    if (major_size == limit_index) {
      throw std::overflow_error("Too many major elements added.");
    }
    no_major_with_elements_unfinished = true;

    offset_to_length_of_current_major +=
        data[offset_to_length_of_current_major].i + 1;

    data.emplace_back();
    ++major_size;
  }

  // The scale_factor is used to scale the elements in the transpose,
  // that  scale_factor * A_ij = T_ji where
  // A is this matrix and T is the computed tranpose of A.
  MajorMinorMatrix compute_transpose(double scale_factor = 1) const {
    auto const el_data_end_it = data.cend() - 1;
    assert(el_data_end_it->i == 0);

    auto const &transposed_major_size = minor_size;
    std::vector<Index> major_structure(transposed_major_size + 1, 0);
    // used later when calculating positions of elements in data
    major_structure[0] = 1;

    // TODO make these scopes functions?

    Index used_majors = 0;
    size_t used_elements = 0;
    Index transposed_used_majors = 0;
    {
      auto data_it = data.cbegin();
      for (; data_it != el_data_end_it; ++used_majors) {
        Index row_nnz = data_it->i;
        used_elements += row_nnz;
        for (; row_nnz > 0; --row_nnz) {
          ++data_it;
          assert(data_it->i < minor_size);
          transposed_used_majors =
              std::max(transposed_major_size, data_it->i + 1);
          ++major_structure[data_it->i + 1];
        }
        ++data_it;
      }

      major_structure.resize(transposed_used_majors + 1);
    }

    // TODO: check overflow?
    size_t transposed_data_size = used_elements + transposed_used_majors + 1;
    Storage transposed_data(transposed_data_size);

    {  // fill in lengths of new majors for the transposed matrix
      size_t pos_of_major_len = 0;
      for (auto it = ++major_structure.cbegin(); it != major_structure.cend();
           ++it) {
        assert(pos_of_major_len < transposed_data.size());
        transposed_data[pos_of_major_len].i = *it;
        pos_of_major_len += *it + 1;
      }
    }

    // calculate start position for the elements of each major in the transposed
    // matrix.
    // now we use the first element of major_structure
    std::partial_sum(major_structure.begin(), major_structure.end(),
                     major_structure.begin(),
                     [](auto lhs, auto rhs) { return 1 + lhs + rhs; });
    // reference to denote new purpose after partial sum
    auto &location_to_write_el = major_structure;

    assert(location_to_write_el.back() == transposed_data.size());

    {  // fill in elements
      auto data_it = data.cbegin();

      for (Index major_index = 0; data_it != el_data_end_it; ++major_index) {
        for (Index row_nnz = data_it->i; row_nnz > 0; --row_nnz) {
          ++data_it;
          auto const pos_for_el = location_to_write_el[data_it->i]++;
          transposed_data[pos_for_el] = {major_index,
                                         scale_factor * data_it->v};
          assert(major_index < used_majors);
        }
        ++data_it;
      }
    }

    {  // sort minor elements in each major
      auto const el_transposed_data_end_it = transposed_data.cend() - 1;

      auto transposed_data_it = transposed_data.begin();
      for (; transposed_data_it != el_transposed_data_end_it;) {
        const Index row_nnz = transposed_data_it->i;
        auto end_of_major_it = transposed_data_it + 1 + row_nnz;
        std::sort(transposed_data_it + 1, end_of_major_it,
                  [](auto lhs, auto rhs) { return lhs.i < rhs.i; });
        transposed_data_it = end_of_major_it;
      }

      assert(transposed_data_it + 1 == transposed_data.cend());
    }

    return MajorMinorMatrix(minor_size, major_size, nnz,
                            transposed_data.size() - 1, true,
                            std::move(transposed_data));
  }

  void shrink_to_fit() { data.shrink_to_fit(); }
};
}
}
