// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "CommonTypes.hpp"
#include "MajorMinorMatrix.hpp"
#include "SparseMatrixGenerator.hpp"
#include "convert_matrix_types_util.hpp"

#include "Eigen/Dense"

#include "gtest/gtest.h"

#include <iostream>

namespace ppilps {
namespace test {

using namespace sparse_math;

void compare_transpose_with_eigen(unsigned int const major_size,
                                  unsigned int const minor_size) {
  auto const base_m = Eigen::MatrixXd::Random(major_size, minor_size).eval();

  SparseMatrixGenerator gen(base_m);

  while (true) {
    auto const &m = gen.get_matrix();

    auto const result = m.transpose();

    MajorMinorMatrix my_m;
    convert_from_eigen_matrix(m, major_size, my_m);
    assert(my_m.get_major_size() == major_size);

    const auto my_m_transposed = my_m.compute_transpose();

    Eigen::MatrixXd my_result_converted;
    convert_to_eigen_matrix(my_m_transposed, my_result_converted);

    ASSERT_TRUE(result.isApprox(my_result_converted)) << "Eigen's tranpose m:\n"
                                                      << result
                                                      << "\n\n we computed\n"
                                                      << my_result_converted;
    if (gen.is_done()) {
      break;
    } else {
      gen.next();
    }
  }
}

TEST(MajorMinorMatrix, transpose__for_tiny_random_matrix__same_as_eigen) {
  std::cout << "test matrix sizes:";

  for (int major_size = 0; major_size < 4; ++major_size) {
    for (int minor_size = 0; minor_size < 4; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), ";
      compare_transpose_with_eigen(major_size, minor_size);
    }
  }

  std::cout << std::endl;
}

TEST(MajorMinorMatrix,
     transpose__for_small_random_matrix__same_as_eigen__slow) {
  std::cout << "test matrix sizes:";

  for (int major_size = 0; major_size < 4; ++major_size) {
    for (int minor_size = 4; minor_size < 8; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), "
                << std::flush;
      compare_transpose_with_eigen(major_size, minor_size);
    }
  }

  for (int major_size = 4; major_size < 8; ++major_size) {
    for (int minor_size = 0; minor_size < 4; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), "
                << std::flush;
      compare_transpose_with_eigen(major_size, minor_size);
    }
  }

  for (int major_size = 4; major_size < 7; ++major_size) {
    for (int minor_size = 4; minor_size < 7; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), "
                << std::flush;
      compare_transpose_with_eigen(major_size, minor_size);
    }
  }

  std::cout << std::endl;
}
}
}
