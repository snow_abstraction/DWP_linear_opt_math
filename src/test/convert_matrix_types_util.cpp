// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "convert_matrix_types_util.hpp"
#include "MajorMinorMatrix.hpp"

namespace ppilps {
namespace test {

using Index = sparse_math::Index;

void convert_from_eigen_matrix(Eigen::MatrixXd const &from_matrix,
                               unsigned int const major_size_used,
                               sparse_math::MajorMinorMatrix &to_matrix) {
  to_matrix.clear();

  // treat rows as major
  auto const major_size = from_matrix.rows();
  auto const minor_size = from_matrix.cols();

  assert(major_size_used <= major_size);

  for (unsigned int i = 0; i < major_size_used; ++i) {
    for (int j = 0; j < minor_size; ++j) {
      if (from_matrix(i, j) != 0.) {
        to_matrix.add_element_to_current_major(j, from_matrix(i, j));
      }
    }

    to_matrix.finish_major();
  }

  to_matrix.increase_major_size_to(major_size);
  to_matrix.increase_minor_size_to(minor_size);
}

void convert_to_eigen_matrix(sparse_math::MajorMinorMatrix const &from_matrix,
                             Eigen::MatrixXd &to_matrix) {
  // treat rows as major
  auto const num_major = from_matrix.get_major_size();
  auto const num_minor = from_matrix.get_minor_size();

  to_matrix.resize(num_major, num_minor);

  if (num_major == 0 || num_minor == 0) {
    return;
  }

  to_matrix.setZero();

  auto const el_data_end_it = from_matrix.get_data().cend() - 1;
  auto element_it = from_matrix.get_data().cbegin();
  for (Index row_index = 0; element_it != el_data_end_it; ++row_index) {
    for (Index nnz = element_it->i; nnz > 0; --nnz) {
      ++element_it;
      assert(row_index < num_major);
      to_matrix(row_index, element_it->i) = element_it->v;
    }
    ++element_it;
  }
}
}
}
