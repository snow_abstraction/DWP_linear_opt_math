// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "Eigen/Dense"
#include "MajorMinorMatrix.hpp"

namespace ppilps {
namespace test {

void convert_from_eigen_matrix(Eigen::MatrixXd const &from_matrix,
                               unsigned int const major_size_used,
                               sparse_math::MajorMinorMatrix &to_matrix);

void convert_to_eigen_matrix(sparse_math::MajorMinorMatrix const &from_matrix,
                             Eigen::MatrixXd &to_matrix);
}
}
