// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//#include "CommonTypes.hpp"
#include "MajorMinorMatrix.hpp"
#include "SparseMatrixGenerator.hpp"
#include "convert_matrix_types_util.hpp"
#include "operations.hpp"

#include "Eigen/Dense"

#include "gtest/gtest.h"

#include <iostream>

namespace ppilps {
namespace test {

using namespace sparse_math;

void compare_multiplication_with_eigen(unsigned int const major_size,
                                       unsigned int const minor_size) {
  auto const base_m = Eigen::MatrixXd::Random(major_size, minor_size).eval();
  auto const v = Eigen::VectorXd::Random(minor_size).eval();

  std::vector<double> my_v(v.size());
  Eigen::VectorXd::Map(my_v.data(), v.size()) = v;

  MajorMinorMatrix my_m;
  SparseMatrixGenerator gen(base_m);

  while (true) {
    auto const &m = gen.get_matrix();
    auto const result = m * v;

    my_m.clear();
    convert_from_eigen_matrix(m, gen.get_major_size_used(), my_m);
    assert(my_m.get_major_size() == major_size);

    // some debugging code
    // {
    //   Eigen::MatrixXd back_again;
    //   convert_to_eigen_matrix(my_m, back_again);
    //   ASSERT_TRUE(m == back_again);
    // }

    std::vector<double> my_result(my_m.get_major_size(), -1.);
    multiply(my_m, my_v, my_result);

    Eigen::VectorXd my_result_converted;
    my_result_converted.resize(my_result.size());
    for (size_t my_result_idx = 0; my_result_idx < my_result.size();
         ++my_result_idx) {
      my_result_converted(my_result_idx) = my_result[my_result_idx];
    }

    auto precision = Eigen::NumTraits<double>::dummy_precision();
    if (major_size * minor_size > 14) {
      // a hack, instead we could analysze matrix to
      // compute the expected percision
      precision = 0.00000001;
    }

    ASSERT_TRUE(result.isApprox(my_result_converted, precision))
        << "Matrix m:\n"
        << m << "\nVector v:\n"
        << v << "\n\neigen computed:\n"
        << result << "\n\n we computed\n"
        << my_result_converted;

    if (gen.is_done()) {
      break;
    } else {
      gen.next();
    }
  }
}

TEST(MajorMinorMatrix, multiply__for_tiny_random_matrices__same_as_eigen) {
  std::cout << "test matrix sizes:";
  for (int major_size = 0; major_size < 4; ++major_size) {
    for (int minor_size = 0; minor_size < 6; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), ";
      compare_multiplication_with_eigen(major_size, minor_size);
    }
  }

  std::cout << std::endl;
}

// The point is to test that loop unrolling (for SIMD) in multiply works.
TEST(MajorMinorMatrix,
     multiply__for_small_random_matrices__same_as_eigen__slow) {
  std::cout << "test matrix sizes:";
  for (int major_size = 1; major_size < 3; ++major_size) {
    for (int minor_size = 6; minor_size < 10; ++minor_size) {
      std::cout << "(" << major_size << "," << minor_size << "), "
                << std::flush;
      compare_multiplication_with_eigen(major_size, minor_size);
    }
  }

  std::cout << std::endl;
}
}
}
