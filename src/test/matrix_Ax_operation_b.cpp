// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//#include "CommonTypes.hpp"
#include "MajorMinorMatrix.hpp"
#include "SparseMatrixGenerator.hpp"
#include "convert_matrix_types_util.hpp"
#include "operations.hpp"

#include "Eigen/Dense"

#include "gtest/gtest.h"

#include <algorithm>
#include <cmath>
#include <random>

namespace ppilps {
namespace test {

using namespace sparse_math;

uint64_t compute_max_for_sparsity_pattern_skip(unsigned int const major_size,
                                               unsigned int const minor_size) {
  // compute an upper bound for random distribution
  // such that we expected to try about 128 patterns for bigger matrices

  // E[number_patterns / average_patterns_increment] = approx. =
  // 2^(major_size * minor_size / (0.5 * 2^(major_size * minor_size - 6) =
  // 2^(major_size * minor_size / (2^(major_size * minor_size - 7) =
  // 2^7 = 128

  assert(major_size * minor_size <= 64);
  auto const upper_bound_for_bigger_matrices =
      major_size * minor_size <= 6
          ? 0
          : std::exp2<uint64_t>(major_size * minor_size - 6);
  auto const upper_bound =
      std::max<uint64_t>(1, upper_bound_for_bigger_matrices);

  return upper_bound;
}

template <class MyOperation, class EigenOperation>
void compare_Ax_operation_b_with_eigen(unsigned int const major_size,
                                       unsigned int const minor_size,
                                       MyOperation my_op,
                                       EigenOperation eigen_op) {
  std::default_random_engine generator;
  // used to skip sparsity patterns to speed up tests
  auto const upper_bound =
      compute_max_for_sparsity_pattern_skip(major_size, minor_size);
  std::uniform_int_distribution<uint64_t> sparisty_pattern_skip_dist(
      1, upper_bound);

  auto const base_A = Eigen::MatrixXd::Random(major_size, minor_size).eval();
  auto const x = Eigen::VectorXd::Random(minor_size).eval();
  auto const b = Eigen::VectorXd::Random(major_size).eval();

  std::vector<double> my_x(x.size());
  Eigen::VectorXd::Map(my_x.data(), x.size()) = x;

  std::vector<double> my_b(b.size());
  Eigen::VectorXd::Map(my_b.data(), b.size()) = b;

  std::vector<double> my_result(major_size);

  MajorMinorMatrix my_A;
  SparseMatrixGenerator gen(base_A);

  while (true) {
    auto result = Eigen::VectorXd::Random(major_size).eval();
    Eigen::VectorXd::Map(my_result.data(), my_result.size()) = result;

    auto const &A = gen.get_matrix();

    eigen_op(A, x, b, result);

    convert_from_eigen_matrix(A, gen.get_major_size_used(), my_A);
    assert(my_A.get_major_size() == major_size);

    my_op(my_A, my_x, my_b, my_result);

    Eigen::VectorXd my_result_converted;
    my_result_converted.resize(my_result.size());
    for (size_t my_result_idx = 0; my_result_idx < my_result.size();
         ++my_result_idx) {
      my_result_converted(my_result_idx) = my_result[my_result_idx];
    }

    ASSERT_TRUE(result.isApprox(my_result_converted))
        << "Sparsity pattern:\n"
        << gen.get_sparsity_pattern() << "\nMatrix A\n"
        << A << "\nVector x:\n"
        << x << "\nVector b:\n"
        << b << "\n\nEigen computed:\n"
        << result << "\n\n we computed\n"
        << my_result_converted;

    if (gen.is_done()) {
      break;
    } else {
      gen.advance(sparisty_pattern_skip_dist(generator));
    }
  }
}

TEST(MajorMinorMatrix, Ax_plus_b__for_small_random_matrices__same_as_eigen) {
  for (int major_size = 0; major_size < 9; ++major_size) {
    for (int minor_size = 0; minor_size < 9; ++minor_size) {
      compare_Ax_operation_b_with_eigen(
          major_size, minor_size, Ax_plus_b,
          [](auto &A, auto &x, auto &b, auto &result) { result = A * x + b; });
    }
  }
}

TEST(MajorMinorMatrix, Ax_minus_b__for_small_random_matrices__same_as_eigen) {
  for (int major_size = 0; major_size < 9; ++major_size) {
    for (int minor_size = 0; minor_size < 9; ++minor_size) {
      compare_Ax_operation_b_with_eigen(
          major_size, minor_size, Ax_minus_b,
          [](auto &A, auto &x, auto &b, auto &result) { result = A * x - b; });
    }
  }
}

TEST(MajorMinorMatrix,
     output_plus_Ax_plus_b__for_small_random_matrices__same_as_eigen) {
  for (int major_size = 0; major_size < 9; ++major_size) {
    for (int minor_size = 0; minor_size < 9; ++minor_size) {
      compare_Ax_operation_b_with_eigen(
          major_size, minor_size, output_plus_Ax_plus_b,
          [](auto &A, auto &x, auto &b, auto &result) {
            result = (result + A * x + b).eval();
          });
    }
  }
}

TEST(MajorMinorMatrix,
     max_0_and_Ax_minus_b__for_small_random_matrices__same_as_eigen) {
  for (int major_size = 0; major_size < 9; ++major_size) {
    for (int minor_size = 0; minor_size < 9; ++minor_size) {
      compare_Ax_operation_b_with_eigen(
          major_size, minor_size, max_0_and_Ax_minus_b,
          [](auto &A, auto &x, auto &b, auto &result) {
            result = (A * x - b).array().max(0.0);
          });
    }
  }
}
}
}
