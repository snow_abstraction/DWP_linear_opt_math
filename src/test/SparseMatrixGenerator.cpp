// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "SparseMatrixGenerator.hpp"

namespace ppilps {
namespace test {

SparseMatrixGenerator::SparseMatrixGenerator(Eigen::MatrixXd const &base_matrix)
    : base_matrix(base_matrix),
      major_size(base_matrix.rows()),
      minor_size(base_matrix.cols()),
      // should be major_size * minor numbers of one bits
      sparsity_pattern_last(major_size * minor_size > 63
                                ? -1
                                : (1 << (major_size * minor_size)) - 1),
      matrix(major_size, minor_size) {
  assert(minor_size * major_size <= 64);
  setup_matrix_and_more_for_sparsity_pattern();
}

void SparseMatrixGenerator::next() {
  assert(sparsity_pattern != sparsity_pattern_last);
  ++sparsity_pattern;
  setup_matrix_and_more_for_sparsity_pattern();
}

void SparseMatrixGenerator::advance(uint64_t places) {
  assert(places == 0 || sparsity_pattern != sparsity_pattern_last);
  uint64_t const new_pattern = sparsity_pattern + places;
  bool const has_wrapped = new_pattern < sparsity_pattern;
  bool const past_last = sparsity_pattern_last < new_pattern;

  if (has_wrapped || past_last) {
    sparsity_pattern = sparsity_pattern_last;
  } else {
    sparsity_pattern = new_pattern;
  }

  setup_matrix_and_more_for_sparsity_pattern();
}

void SparseMatrixGenerator::setup_matrix_and_more_for_sparsity_pattern() {
  major_size_used = 0;
  for (unsigned int i = 0; i < major_size; ++i) {
    for (unsigned int j = 0; j < minor_size; ++j) {
      if (sparsity_pattern & (1 << (i * minor_size + j))) {
        major_size_used = std::max(major_size_used, i + 1);
        matrix(i, j) = base_matrix(i, j);
      } else {
        matrix(i, j) = 0.0;
      }
    }
  }
}
}
}
