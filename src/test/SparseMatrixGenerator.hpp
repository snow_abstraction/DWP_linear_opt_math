// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "Eigen/Dense"

namespace ppilps {
namespace test {

// Generates a sequence of matrices consisting
// of possible sparsity patterns by copying
// elements "base matrix" supplied to the constructor
// for the current sparsity pattern.
// Calling next() after all going through all patterns is
// an error.
//
// Providing a real iterator instead was deemed overkill.
class SparseMatrixGenerator {
 public:
  SparseMatrixGenerator() = delete;
  SparseMatrixGenerator(Eigen::MatrixXd const &base_matrix);
  Eigen::MatrixXd const &get_matrix() const { return matrix; }

  // one plus the largest major/row number with a nonzero element
  unsigned int get_major_size_used() const { return major_size_used; }
  bool is_done() const { return sparsity_pattern == sparsity_pattern_last; }
  void next();
  void advance(uint64_t places);

  uint64_t get_sparsity_pattern() const { return sparsity_pattern; }

 private:
  Eigen::MatrixXd const base_matrix;
  // treat as major
  unsigned int const major_size;
  unsigned int const minor_size;
  uint64_t const sparsity_pattern_last;

  Eigen::MatrixXd matrix;
  // the bits in sparsity represent a major_size-by-minor_size binary matrix
  // where 0 indicates that the test martix should also be zero
  uint64_t sparsity_pattern = 0;
  unsigned int major_size_used = 0;

  void setup_matrix_and_more_for_sparsity_pattern();
};
}
}
