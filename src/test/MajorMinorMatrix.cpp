// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//#include "CommonTypes.hpp"
#include "MajorMinorMatrix.hpp"

#include "gtest/gtest.h"

namespace ppilps {
namespace test {

using namespace sparse_math;

TEST(MajorMinorMatrix, get_nnz__after_adding_elements__nnz_as_expected) {
  MajorMinorMatrix m;
  ASSERT_EQ(m.get_nnz(), 0);

  m.finish_major();
  ASSERT_EQ(m.get_nnz(), 0);

  m.add_element_to_current_major(0, 1);
  ASSERT_EQ(m.get_nnz(), 1);

  m.finish_major();
  m.add_element_to_current_major(0, 2);
  m.finish_major();
  ASSERT_EQ(m.get_nnz(), 2);
}

TEST(MajorMinorMatrix,
     get_nnz__after_compute_transpose__nnz_same_in_transposed) {
  MajorMinorMatrix m;
  m.finish_major();
  m.add_element_to_current_major(0, 1);
  m.add_element_to_current_major(2, 1);
  m.finish_major();
  m.add_element_to_current_major(0, 2);
  m.finish_major();
  auto m_t = m.compute_transpose();

  ASSERT_EQ(m.get_nnz(), m_t.get_nnz());
}

TEST(
    MajorMinorMatrix,
    add_element_to_current_major__after_compute_transpose__matrix_data_is_correct) {
  MajorMinorMatrix m;
  m.add_element_to_current_major(0, 2);
  m.finish_major();
  auto m_t = m.compute_transpose();
  m_t.add_element_to_current_major(2, 3);
  m_t.finish_major();

  MajorMinorMatrix::Storage const expected_data = {
      {1, 0}, {0, 2}, {1, 0}, {2, 3}, {0, 0}};

  ASSERT_EQ(expected_data, m_t.get_data());
  ASSERT_EQ(2, m_t.get_major_size());
  ASSERT_EQ(3, m_t.get_minor_size());
  ASSERT_EQ(2, m_t.get_nnz());
}
}
}
