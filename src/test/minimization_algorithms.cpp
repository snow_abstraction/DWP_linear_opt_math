// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical
// optimization.
//
// Copyright (C) 2017-2018 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//#include "CommonTypes.hpp"

#include "minimization_algorithms.hpp"
#include "MajorMinorMatrix.hpp"
#include "logging.hpp"
#include "problem_definitions.hpp"

#include "gtest/gtest.h"

#include <vector>

namespace ppilps {
namespace test {

using namespace sparse_math;
using namespace optimization;
using namespace logging;

TEST(
    algorithm,
    find_feasible_point__upper_bound_less_than_region_from_inequality_constraints__terminates_without_feasible_point) {
  // Ax = b
  MajorMinorMatrix A;
  A.increase_minor_size_to(1);
  std::vector<double> b;

  // Cx <= d
  // -1x <= -2   <==>  x >= 2
  MajorMinorMatrix C;
  C.add_element_to_current_major(0, -1);
  C.finish_major();
  std::vector<double> d = {-2};

  // lb <= x <= ub
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  DevNullLog log;
  LinearFeasibilityProblemInstance const problem(A, b, C, d, bounds);

  auto rv = find_feasible_point(problem, 0.0001, 0.0001, log);

  ASSERT_EQ(rv.reason,
            Result_of_find_feasible_point::StopReason::feasible_gradient_small);
  ASSERT_TRUE(rv.feasible_point_found.empty());
}

TEST(
    algorithm,
    find_feasible_point__lower_bound_greater_than_region_from_inequality_constraints__terminates_without_feasible_point) {
  // Ax = b
  MajorMinorMatrix A;
  A.increase_minor_size_to(1);
  std::vector<double> b;

  // Cx <= d
  // 1x <= -2
  MajorMinorMatrix C;
  C.add_element_to_current_major(0, 1);
  C.finish_major();
  std::vector<double> d = {-2};

  // lb <= x <= ub
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  DevNullLog log;
  LinearFeasibilityProblemInstance const problem(A, b, C, d, bounds);

  auto rv = find_feasible_point(problem, 0.0001, 0.0001, log);

  ASSERT_EQ(rv.reason,
            Result_of_find_feasible_point::StopReason::feasible_gradient_small);
  ASSERT_TRUE(rv.feasible_point_found.empty());
}

TEST(
    algorithm,
    find_feasible_point__upper_bound_less_than_region_from_equality_constraints__terminates_without_feasible_point) {
  // Ax = b
  MajorMinorMatrix A;
  A.add_element_to_current_major(0, 1);
  A.finish_major();
  std::vector<double> b = {3};

  // Cx <= d
  MajorMinorMatrix C;
  C.increase_minor_size_to(1);
  std::vector<double> d;

  // lb <= x <= ub
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  DevNullLog log;
  LinearFeasibilityProblemInstance const problem(A, b, C, d, bounds);

  auto rv = find_feasible_point(problem, 0.0001, 0.0001, log);

  ASSERT_EQ(rv.reason,
            Result_of_find_feasible_point::StopReason::feasible_gradient_small);
  ASSERT_TRUE(rv.feasible_point_found.empty());
}

TEST(
    algorithm,
    find_feasible_point__lower_bound_greater_than_region_from_equality_constraints__terminates_without_feasible_point) {
  // Ax = b
  MajorMinorMatrix A;
  A.add_element_to_current_major(0, 1);
  A.finish_major();
  std::vector<double> b = {-3};

  // Cx <= d
  MajorMinorMatrix C;
  C.increase_minor_size_to(1);
  std::vector<double> d;

  // lb <= x <= ub
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  DevNullLog log;
  LinearFeasibilityProblemInstance const problem(A, b, C, d, bounds);

  auto rv = find_feasible_point(problem, 0.0001, 0.0001, log);

  ASSERT_EQ(rv.reason,
            Result_of_find_feasible_point::StopReason::feasible_gradient_small);
  ASSERT_TRUE(rv.feasible_point_found.empty());
}

TEST(
    algorithm,
    find_feasible_point__supplied_start_point_is_outside_of_feasible_region__terminates_with_expected_feasible_point) {
  // Ax = b
  MajorMinorMatrix A;
  A.increase_minor_size_to(1);
  std::vector<double> b;

  // define constraints for x in [1, 3]
  // Cx <= d
  MajorMinorMatrix C;
  C.add_element_to_current_major(0, -1);
  C.finish_major();
  C.add_element_to_current_major(0, 1);
  C.finish_major();
  std::vector<double> d = {-1, 3};

  // lb <= x <= ub
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  DevNullLog log;
  LinearFeasibilityProblemInstance const problem(A, b, C, d, bounds);

  std::vector<double> const start = {6};
  auto rv = find_feasible_point(problem, 0.0001, 0.0001, log, 100, start);

  ASSERT_EQ(Result_of_find_feasible_point::StopReason::feasible_gradient_small,
            rv.reason);

  ASSERT_FALSE(rv.feasible_point_found.empty());
  ASSERT_FLOAT_EQ(1, rv.feasible_point_found[0]);
}

TEST(
    algorithm,
    is_feasible_gradient_small_in_max_norm__on_upper_bound_and_large_positive_gradient__returns_false) {
  double const threashold_for_small_gradient = 0.01;
  std::vector<double> const gradient = {1};
  std::vector<double> const x = {1};
  std::vector<TwoSidedVariableBounds> bounds = {{0, 1}};

  ASSERT_FALSE(is_feasible_gradient_small_in_max_norm(
      threashold_for_small_gradient, gradient, x, bounds));
}
}
}
