// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical optimization.
//
// Copyright (C) 2017 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "problem_definitions.hpp"

#include <assert.h>

namespace FindFeasible {
namespace Optimization {

TwoSidedVariableBounds::TwoSidedVariableBounds(double const lb_in,
                                               double const ub_in)
    : lb(lb_in), ub(ub_in) {
  assert(lb <= ub);
}

LPInstance::LPInstance(
    std::vector<double> const &c_in, SparseMath::MajorMinorMatrix const &A_in,
    std::vector<double> const &b_in, SparseMath::MajorMinorMatrix const &C_in,
    std::vector<double> const &d_in,
    std::vector<TwoSidedVariableBounds> const &variable_bounds_in)
    : c(c_in),
      A(A_in),
      b(b_in),
      C(C_in),
      d(d_in),
      variable_bounds(variable_bounds_in) {
  assert(A.get_minor_size() == c.size());
  assert(A.get_major_size() == b.size());
  assert(C.get_major_size() == d.size());
  assert(A.get_minor_size() == C.get_minor_size());
  assert(A.get_minor_size() == variable_bounds.size());
}

LinearFeasibilityProblemInstance::LinearFeasibilityProblemInstance(
    SparseMath::MajorMinorMatrix const &A_in, std::vector<double> const &b_in,
    SparseMath::MajorMinorMatrix const &C_in, std::vector<double> const &d_in,
    std::vector<TwoSidedVariableBounds> const &variable_bounds_in)
    : A(A_in), b(b_in), C(C_in), d(d_in), variable_bounds(variable_bounds_in) {
  assert(A.get_major_size() == b.size());
  assert(C.get_major_size() == d.size());
  assert(A.get_minor_size() == C.get_minor_size());
  assert(A.get_minor_size() == variable_bounds.size());
}
}
}
