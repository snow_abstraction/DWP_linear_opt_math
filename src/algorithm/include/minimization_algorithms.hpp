// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical
// optimization.
//
// Copyright (C) 2017-2018 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include "problem_definitions.hpp"

#include <vector>

namespace ppilps {

namespace logging {
class LogBase;
}

namespace optimization {

struct Result_of_find_feasible_point {
  enum class StopReason { feasible_gradient_small, max_iterations };
  StopReason reason;
  // maybe empty if no feasible point found
  std::vector<double> feasible_point_found;
};

Result_of_find_feasible_point find_feasible_point(
    LinearFeasibilityProblemInstance const &problem,
    double const max_norm_threashold_for_small_gradient_stopping_criteria,
    double const allowed_constraint_violation, logging::LogBase &log,
    uint64_t const max_iterations = 1e10,
    // The suggested_start_point is empty, the algorithm should find its
    // own. If not empty, the dimensions must match the problems and it will
    // project it into the feasible region defined by the variable bounds.
    std::vector<double> const &suggested_start_point = {}) noexcept;

// For debugging other methods
void use_hacky_subgradient_method_to_find_vector_minimizing_infeasibility_squared(
    LinearFeasibilityProblemInstance const &problem,
    logging::LogBase &log) noexcept;

// Exposed here for unit testing
bool is_feasible_gradient_small_in_max_norm(
    double const threashold_for_small_gradient,
    std::vector<double> const &gradient, std::vector<double> const &x,
    std::vector<TwoSidedVariableBounds> const &bounds) noexcept;
}
}
