// This file is a part of DWP_linear_opt_math.
// DWP_linear_opt_math is a collection of software related to mathematical
// optimization.
//
// Copyright (C) 2017-2018 Douglas W. Potter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include "minimization_algorithms.hpp"

#include "MajorMinorMatrix.hpp"
#include "operations.hpp"

#include "logging.hpp"

#include <algorithm>
#include <cmath>
#include <limits>

namespace ppilps {
namespace optimization {

using namespace sparse_math;
using namespace logging;

namespace {
inline void follow_gradient_down(std::vector<double> const &prev_x,
                                 std::vector<double> const &gradient,
                                 double const step_size,
                                 std::vector<double> &next_x) noexcept {
  std::transform(prev_x.cbegin(), prev_x.cend(), gradient.cbegin(),
                 next_x.begin(),
                 [step_size](double const &prev_x_i, double const &gradient_i) {
                   return prev_x_i - step_size * gradient_i;
                 });
}

inline void compute_error(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<double> const &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  Ax_minus_b(A, x, b, error_Ax_minus_b);
  max_0_and_Ax_minus_b(C, x, d, error_max_0_and_Cx_minus_d);
}

inline void update_error_and_gradient(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<double> const &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d,
    std::vector<double> &gradient) noexcept {
  Ax_minus_b(A, x, b, error_Ax_minus_b);
  max_0_and_Ax_minus_b(C, x, d, error_max_0_and_Cx_minus_d);
  multiply(A_transposed, error_Ax_minus_b, gradient);
  output_plus_Ax(C_transposed, error_max_0_and_Cx_minus_d, gradient);
}

inline void project_into_bounds(
    std::vector<TwoSidedVariableBounds> const &bounds,
    std::vector<double> &x) noexcept {
  std::transform(x.cbegin(), x.cend(), bounds.cbegin(), x.begin(),
                 [](double const &x_i, TwoSidedVariableBounds const &bounds_i) {
                   return std::min(std::max(x_i, bounds_i.lb), bounds_i.ub);
                 });
}

inline bool is_within_bounds(std::vector<TwoSidedVariableBounds> const &bounds,
                             std::vector<double> const &x) noexcept {
  return std::equal(
      x.cbegin(), x.cend(), bounds.cbegin(),
      [](double const &x_i, TwoSidedVariableBounds const &bounds_i) {
        return bounds_i.lb <= x_i && x_i <= bounds_i.ub;
      });
}

inline double compute_self_dot_product(std::vector<double> const &x) noexcept {
  return std::inner_product(x.begin(), x.end(), x.begin(), 0.0);
}

inline double compute_l2_norm(std::vector<double> const &x) {
  return std::sqrt(compute_self_dot_product(x));
}

inline double compute_objective_value_from_error(
    std::vector<double> const &error_Ax_minus_b,
    std::vector<double> const &error_max_0_and_Cx_minus_d) noexcept {
  double const objective_value =
      (0.5 * compute_self_dot_product(error_Ax_minus_b) +
       0.5 * compute_self_dot_product(error_max_0_and_Cx_minus_d));

  return objective_value;
}

inline double compute_objective_value_and_update_error(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<double> const &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  compute_error(A, b, C, d, A_transposed, C_transposed, x, error_Ax_minus_b,
                error_max_0_and_Cx_minus_d);
  double const objective_value = compute_objective_value_from_error(
      error_Ax_minus_b, error_max_0_and_Cx_minus_d);

  return objective_value;
}

// TODO move definition to cpp file
// TODO: C++17, use structured bindings to return the objective value and
// vector
double compute_feasible_start_point(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &variable_bounds,
    std::vector<double> const &suggested_start_point,
    std::vector<double> &start_point, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) {
  if (!suggested_start_point.empty()) {
    start_point = suggested_start_point;
    project_into_bounds(variable_bounds, start_point);
    return compute_objective_value_and_update_error(
        A, b, C, d, A_transposed, C_transposed, start_point, error_Ax_minus_b,
        error_max_0_and_Cx_minus_d);
  }

  std::vector<double> middle;
  middle.reserve(variable_bounds.size());
  std::transform(variable_bounds.begin(), variable_bounds.end(),
                 std::back_inserter(middle), [](auto const &bound) {
                   return 0.5 * bound.lb + 0.5 * bound.ub;
                 });
  double const objective_value_at_middle =
      compute_objective_value_and_update_error(
          A, b, C, d, A_transposed, C_transposed, middle, error_Ax_minus_b,
          error_max_0_and_Cx_minus_d);

  std::vector<double> projected_zero(variable_bounds.size(), 0);
  project_into_bounds(variable_bounds, projected_zero);
  double const objective_value_at_zero =
      compute_objective_value_and_update_error(
          A, b, C, d, A_transposed, C_transposed, projected_zero,
          error_Ax_minus_b, error_max_0_and_Cx_minus_d);

  double objective_value_at_start_point = std::numeric_limits<double>::max();
  if (objective_value_at_middle < objective_value_at_zero) {
    start_point = middle;
    objective_value_at_start_point = objective_value_at_middle;
  } else {
    start_point = projected_zero;
    objective_value_at_start_point = objective_value_at_zero;
  }

  return objective_value_at_start_point;
}

inline double compute_x_using_backtracking_and_return_objective_value_at_x_core(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &variable_bounds,
    std::vector<double> const &y, std::vector<double> const &gradient_at_y,
    double const objective_value_at_y, double &step_length,
    std::vector<double> &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  size_t const num_variables = variable_bounds.size();

  double objective_value_at_x = std::numeric_limits<double>::max();
  // TODO: is 64 excessive?
  for (int smaller_step_try = 0; smaller_step_try < 64; ++smaller_step_try) {
    follow_gradient_down(y, gradient_at_y, step_length, x);
    project_into_bounds(variable_bounds, x);
    objective_value_at_x = compute_objective_value_and_update_error(
        A, b, C, d, A_transposed, C_transposed, x, error_Ax_minus_b,
        error_max_0_and_Cx_minus_d);

    // <curr_x - y, gradient> + (step/2) * ||curr_x - y||^2
    double const step_term = 0.5 / step_length;
    double approx_term = 0;
    for (size_t i = 0; i < num_variables; ++i) {
      double const diff = x[i] - y[i];
      approx_term += diff * gradient_at_y[i] + step_term * diff * diff;
    }

    if (objective_value_at_x <= objective_value_at_y + approx_term) {
      break;
    }

    // TODO: do we need to watch out for subnormals? flush?
    step_length *= 0.5;
  }

  return objective_value_at_x;
}

inline double compute_x_using_backtracking_and_return_objective_value_at_x(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &variable_bounds,
    std::vector<double> const &y, std::vector<double> &gradient,
    double &step_length, std::vector<double> &x,
    std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  update_error_and_gradient(A, b, C, d, A_transposed, C_transposed, y,
                            error_Ax_minus_b, error_max_0_and_Cx_minus_d,
                            gradient);

  double const objective_value_at_y = compute_objective_value_from_error(
      error_Ax_minus_b, error_max_0_and_Cx_minus_d);

  return compute_x_using_backtracking_and_return_objective_value_at_x_core(
      A, b, C, d, A_transposed, C_transposed, variable_bounds, y, gradient,
      objective_value_at_y, step_length, x, error_Ax_minus_b,
      error_max_0_and_Cx_minus_d);
}

double compute_max_norm(std::vector<double> const &x) noexcept {
  return std::accumulate(x.begin(), x.end(), 0.0, [](auto acc, auto x_el) {
    return std::max(acc, std::abs(x_el));
  });
}

bool stop_due_to_position_and_gradient(
    double const max_norm_threashold_for_small_gradient_stopping_criteria,
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &bounds,
    std::vector<double> const &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d,
    std::vector<double> &gradient) noexcept {
  // input gradient might not be at x so recalculate at x
  update_error_and_gradient(A, b, C, d, A_transposed, C_transposed, x,
                            error_Ax_minus_b, error_max_0_and_Cx_minus_d,
                            gradient);

  return is_feasible_gradient_small_in_max_norm(
      max_norm_threashold_for_small_gradient_stopping_criteria, gradient, x,
      bounds);
}

bool stop_due_to_position_and_gradient_and_handle_logging(
    uint64_t const iteration,
    double const max_norm_threashold_for_small_gradient_stopping_criteria,
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &bounds,
    std::vector<double> const &x, std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d,
    std::vector<double> &gradient, logging::LogBase &log) noexcept {
  double const max_norm_of_gradient = compute_max_norm(gradient);
  double const l2_norm_of_error = compute_l2_norm(error_Ax_minus_b) +
                                  compute_l2_norm(error_max_0_and_Cx_minus_d);

  log.log_comma_separated(LOG_EXPR(iteration), LOG_EXPR(l2_norm_of_error),
                          LOG_EXPR(max_norm_of_gradient));
  log << end_entry;

  bool const stop = stop_due_to_position_and_gradient(
      max_norm_threashold_for_small_gradient_stopping_criteria, A, b, C, d,
      A_transposed, C_transposed, bounds, x, error_Ax_minus_b,
      error_max_0_and_Cx_minus_d, gradient);

  if (stop) {
    log << "stopping since feasible part of the gradient is small."
        << end_entry;
  }

  return stop;
}

bool any_constraint_with_unallowed_violation(
    double const allowed_constraint_violation, MajorMinorMatrix const &A,
    std::vector<double> const &b, MajorMinorMatrix const &C,
    std::vector<double> const &d, MajorMinorMatrix const &A_transposed,
    MajorMinorMatrix const &C_transposed, std::vector<double> const &x,
    std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  compute_error(A, b, C, d, A_transposed, C_transposed, x, error_Ax_minus_b,
                error_max_0_and_Cx_minus_d);

  for (auto const &e : error_Ax_minus_b) {
    if (std::abs(e) > allowed_constraint_violation) {
      return true;
    }
  }

  for (auto const &e : error_max_0_and_Cx_minus_d) {
    if (e > allowed_constraint_violation) {
      return true;
    }
  }

  return false;
}

void advance_y_lot(double const &prev_t, double const &curr_t,
                   std::vector<double> const &prev_x,
                   std::vector<double> const &curr_x,
                   std::vector<double> &y) noexcept {
  // move to curr_x and bit more in direction from prev_x to curr_x
  double const momentum_factor = (prev_t - 1) / curr_t;

  std::transform(
      curr_x.begin(), curr_x.end(), prev_x.begin(), y.begin(),
      [momentum_factor](double const &curr_xi, double const &prev_xi) {
        return curr_xi + momentum_factor * (curr_xi - prev_xi);
      });
}

void advance_y_little(double const &prev_t, double const &curr_t,
                      std::vector<double> const &prev_x,
                      std::vector<double> const &curr_x,
                      std::vector<double> &y) noexcept {
  // move to prev_x and bit more in direction from prev_x to curr_x
  double const momentum_factor = prev_t / curr_t;

  std::transform(
      curr_x.begin(), curr_x.end(), prev_x.begin(), y.begin(),
      [momentum_factor](double const &curr_xi, double const &prev_xi) {
        return prev_xi + momentum_factor * (curr_xi - prev_xi);
      });
}

void find_feasible_point_core(
    MajorMinorMatrix const &A, std::vector<double> const &b,
    MajorMinorMatrix const &C, std::vector<double> const &d,
    MajorMinorMatrix const &A_transposed, MajorMinorMatrix const &C_transposed,
    std::vector<TwoSidedVariableBounds> const &variable_bounds, double &prev_t,
    double &curr_t, double &step_length, double &best_objective_value,
    std::vector<double> &gradient, std::vector<double> &y,
    std::vector<double> &prev_x, std::vector<double> &curr_x,
    std::vector<double> &error_Ax_minus_b,
    std::vector<double> &error_max_0_and_Cx_minus_d) noexcept {
  assert(is_within_bounds(variable_bounds, prev_x));
  assert(is_within_bounds(variable_bounds, curr_x));

  // Here curr_x plays the role p_{\bar{L}}(y_k) in
  // "Fast Proximal Gradient Method with Backtracking" from Beck.
  double objective_value_at_x =
      compute_x_using_backtracking_and_return_objective_value_at_x(
          A, b, C, d, A_transposed, C_transposed, variable_bounds, y, gradient,
          step_length, curr_x, error_Ax_minus_b, error_max_0_and_Cx_minus_d);

  prev_t = curr_t;
  curr_t = 0.5 + 0.5 * std::sqrt(1 + 4 * curr_t * curr_t);

  // This if ensures that we decrease monotonically
  // Here curr_x plays for the role of z in
  // "Monotone Fast Proximal Gradient Method" from Beck.
  if (objective_value_at_x < best_objective_value) {
    best_objective_value = objective_value_at_x;
    advance_y_lot(prev_t, curr_t, prev_x, curr_x, y);
  } else {
    advance_y_little(prev_t, curr_t, prev_x, curr_x, y);
    curr_x = prev_x;
  }
}
}

bool is_feasible_gradient_small_in_max_norm(
    double const threashold_for_small_gradient,
    std::vector<double> const &gradient, std::vector<double> const &x,
    std::vector<TwoSidedVariableBounds> const &bounds) noexcept {
  assert(0.0 < threashold_for_small_gradient);
  assert(is_within_bounds(bounds, x));

  auto const num_variables = x.size();

  for (size_t i = 0; i < num_variables; ++i) {
    if (threashold_for_small_gradient < gradient[i] && x[i] != bounds[i].lb) {
      // not small positive gradient and not alreading on lb so
      // considered feasible
      return false;
    } else if (-threashold_for_small_gradient > gradient[i] &&
               x[i] != bounds[i].ub) {
      // like logic above but for ub
      return false;
    }
  }

  return true;
}

// This function is for debugging other faster algorithms.
void use_hacky_subgradient_method_to_find_vector_minimizing_infeasibility_squared(
    LinearFeasibilityProblemInstance const &problem,
    logging::LogBase &log) noexcept {
  using namespace logging;

  log << "starting: "
      << "use_hacky_subgradient_method_to_find_vector_minimizing_infeasibility_"
         "squared"
      << endl;

  auto const &A = problem.A;
  auto const &b = problem.b;
  auto const &C = problem.C;
  auto const &d = problem.d;

  auto const A_transposed = A.compute_transpose();
  auto const C_transposed = C.compute_transpose();

  std::vector<double> error_Ax_minus_b(A.get_major_size());
  std::vector<double> error_max_0_and_Cx_minus_d(C.get_major_size());

  std::vector<double> x(std::max(A.get_minor_size(), C.get_minor_size()), 0);
  std::vector<double> gradient(x.size(), 0);

  double const lipschitz_of_gradient =
      compute_square_of_forbenius_norm(A) + compute_square_of_forbenius_norm(C);
  assert(0 < lipschitz_of_gradient && "Is the problem empty?");
  double const step_length = 1 / lipschitz_of_gradient;

  for (int i = 0; i < 100000; ++i) {
    update_error_and_gradient(A, b, C, d, A_transposed, C_transposed, x,
                              error_Ax_minus_b, error_max_0_and_Cx_minus_d,
                              gradient);
    double const l2_norm_of_gradient = compute_l2_norm(gradient);
    if (l2_norm_of_gradient < 1e-10) {
      log << "stopping since gradient is small, gradient norm["
          << l2_norm_of_gradient << "]\n";
      break;
    }

    follow_gradient_down(x, gradient, step_length, x);
    project_into_bounds(problem.variable_bounds, x);
  }

  log << "error at termination["
      << (compute_l2_norm(error_Ax_minus_b) +
          compute_l2_norm(error_max_0_and_Cx_minus_d))
      << "]\n";
}

// minimize
// objective: 0.5*(norm(Ax-b))^2 + 0.5*(norm(max(0, Cx-d)))^2
// where norm is the l2 norm
// using a proximal gradient method.
//
// See
// 1. Nesterov's work
// 2. "Proximal Algorithms" by Neal Parikh and Stephen Boyd
// 3. "Gradient-Based Algorithms with Applications to Signal Recovery
//     Problems" by Amir Beck and Marc Teboulle
//
// The Beck reference is the most concrete inspiration and
// inspires some of the variables names and comments even I
// read the references in the order above. The ideas of
// backtracking and monotonic decrease were inspired by Beck.
// And outer loop which restarts the method with a larger step
// length is my idea and made this algo must faster for my test
// instances. This also makes estimating Lipschitz constant of the
// gradient less important.
//
// It is important for fast convergance that the rhs of each row
// is scaled relative to the stopping condition value
// max_norm_threashold_for_small_gradient_stopping_criteria

Result_of_find_feasible_point find_feasible_point(
    LinearFeasibilityProblemInstance const &problem,
    double const max_norm_threashold_for_small_gradient_stopping_criteria,
    double const allowed_constraint_violation, logging::LogBase &log,
    uint64_t const max_iterations,
    std::vector<double> const &suggested_start_point) noexcept {
  using namespace logging;

  Result_of_find_feasible_point rv;

  // TODO enable this in production
  // maybe via exceptions and then moving no except code
  // into another function
  assert(0 <= max_iterations);
  auto const &A = problem.A;
  auto const &b = problem.b;
  auto const &C = problem.C;
  auto const &d = problem.d;
  auto const &variable_bounds = problem.variable_bounds;

  size_t const num_variables = problem.variable_bounds.size();

  if (!suggested_start_point.empty()) {
    assert(suggested_start_point.size() == num_variables);
  }

  log << "starting: "
      << "use_promixal_method_to_find_vector_minimizing_infeasibility_squared\n"
      << "A :" << A.get_major_size() << ", " << A.get_minor_size()
      << "\nC :" << C.get_major_size() << ", " << C.get_minor_size() << "\n";

  double const lipschitze_of_gradient_estimate =
      compute_square_of_forbenius_norm(A) + compute_square_of_forbenius_norm(C);

  log << "lipschitz_of_gradient [" << lipschitze_of_gradient_estimate << "]\n";

  double step_length = 1 / lipschitze_of_gradient_estimate;

  // suffix t denotes transpose
  auto const A_transposed = A.compute_transpose();
  auto const C_transposed = C.compute_transpose();

  std::vector<double> error_Ax_minus_b(A.get_major_size());
  std::vector<double> error_max_0_and_Cx_minus_d(C.get_major_size());

  // The algorithm needs two earlier points.
  // To save copying (or swapping), the role of x1 and x2 switch
  // every iteration. First x1 is the previous x and x2 is current x
  // and then x2 is the current x and x1 is the previous x, and so on.
  std::vector<double> x1;

  double best_objective_value = compute_feasible_start_point(
      A, b, C, d, A_transposed, C_transposed, problem.variable_bounds,
      suggested_start_point, x1, error_Ax_minus_b, error_max_0_and_Cx_minus_d);
  std::vector<double> x2(x1);
  std::vector<double> y(x1);

  std::vector<double> gradient(num_variables, 0);

  bool is_curr_x_x1 = false;

  uint64_t constexpr max_outer_iterations =
      std::numeric_limits<uint64_t>::max() / 2;
  uint64_t iteration = 0;  // total iteration counter
  for (uint64_t outer_iteration = 10;
       outer_iteration < max_outer_iterations && iteration < max_iterations;
       outer_iteration *= 1.1) {
    double curr_t = 1;
    double prev_t = curr_t;
    step_length *= 2;
    for (uint64_t inner_iteration = 0; inner_iteration < outer_iteration;
         ++inner_iteration) {
      if (iteration++ >= max_iterations) {
        rv.reason = Result_of_find_feasible_point::StopReason::max_iterations;
        break;
      }

      is_curr_x_x1 = !is_curr_x_x1;
      std::vector<double> &curr_x = is_curr_x_x1 ? x1 : x2;
      std::vector<double> &prev_x = !is_curr_x_x1 ? x1 : x2;

      find_feasible_point_core(
          A, b, C, d, A_transposed, C_transposed, variable_bounds, prev_t,
          curr_t, step_length, best_objective_value, gradient, y, prev_x,
          curr_x, error_Ax_minus_b, error_max_0_and_Cx_minus_d);
    }

    if (is_curr_x_x1) {
      x2 = y = x1;
    } else {
      x1 = y = x2;
    }

    if (stop_due_to_position_and_gradient_and_handle_logging(
            iteration, max_norm_threashold_for_small_gradient_stopping_criteria,
            A, b, C, d, A_transposed, C_transposed, variable_bounds, x1,
            error_Ax_minus_b, error_max_0_and_Cx_minus_d, gradient, log)) {
      rv.reason =
          Result_of_find_feasible_point::StopReason::feasible_gradient_small;
      break;
    }
  }

  std::vector<double> &x = is_curr_x_x1 ? x1 : x2;

  bool const all_constraints_with_allowed_violation =
      !any_constraint_with_unallowed_violation(
          allowed_constraint_violation, A, b, C, d, A_transposed, C_transposed,
          x, error_Ax_minus_b, error_max_0_and_Cx_minus_d);

  if (all_constraints_with_allowed_violation) {
    rv.feasible_point_found = x;
  }

  return rv;
}
}
}
